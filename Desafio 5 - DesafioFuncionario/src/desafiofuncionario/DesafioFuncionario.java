
package desafiofuncionario;

import java.util.Scanner;
import java.util.ArrayList;

public class DesafioFuncionario {
        public static float informa(ArrayList<Cargo> a, int b){
        return a.get(b-1).getValor();
    }
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        boolean sair = true;
        int opcao;
        float auxCargo;
        int auxCodigo, auxCodigoC;
        String auxNome;
        int auxInforma;
        
        ArrayList<Cargo> arrayC = new ArrayList<Cargo>();
        ArrayList<Funcionario> arrayF = new ArrayList<Funcionario>();
        ArrayList<Integer> arrayAux = new ArrayList<Integer>();
        
        do{
            try{
                System.out.println("1 - cadastrar os cargos da empresa");
                System.out.println("2 - cadastrar os funcionarios");
                System.out.println("3 - mostrar relatorio");
                System.out.println("4 - mostrar valor pago");
                System.out.println("5 - sair");
                opcao = input.nextInt();
                
                
                switch (opcao) {
                    case 1:
                       
                        System.out.println("\n*************************\n");    
                        System.out.println("digite o valor do cargo: ");
                        auxCargo = input.nextFloat();
                        arrayC.add(new Cargo (auxCargo));
                        System.out.println("\n*************************\n");
                        break;
                        
                    case 2:
                        
                        System.out.println("\n*************************\n");
                        System.out.println("Digite o codigo do funcionario: ");
                        auxCodigo = input.nextInt();
                        if(arrayAux.contains(auxCodigo)){
                            System.out.println("Código já existe.");
                            System.err.println("Tentar novamente!");
                        } else{
                            arrayAux.add(auxCodigo);
                        
                        input.nextLine();
                        System.out.println("Digite o nome do funcionario: ");
                        auxNome = input.nextLine();
                        System.out.println("Digite o codigo do cargo do funcionario: ");
                        auxCodigoC = input.nextInt();
                        
                        arrayF.add(new Funcionario(auxCodigo, auxNome, auxCodigoC));
                        System.out.println("\n*************************\n");
                        }
                        break;
                        
                    case 3:
                        for(int i = 0; i < arrayF.size(); i++) {
                            arrayF.get(i).relatorio(arrayC);
                        }
                        break;
                    case 4:
                        System.out.println("\n*************************\n");
                        System.out.println("Informe o código do cargo para ver o seu valor: ");
                        auxInforma = input.nextInt();
                        System.out.println("\n*************************\n");
                        System.out.println("O valor do código" + auxInforma + " é: " + informa(arrayC, auxInforma));
                        System.out.println("\n*************************\n");
                        break;
                    case 5:
                        sair = false;
                        break;
                    default :
                        System.err.println("opcao invalida");
                        System.out.println("\n*************************\n");
                }
            }
            catch(Exception e) {
                System.err.println("Erro!");
                System.out.println("Verifique se inseriu o código do cargo corretamente, se não, adicione outros cargos");
                input.nextLine();
            } 
        } while (sair);
    }
    
}
