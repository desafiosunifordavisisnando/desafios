
package desafiofuncionario;

public class Cargo {
    
    private float valor;
    
    public Cargo(float valor) {
        this.valor = valor;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }
}
