
package desafiofuncionario;

import java.util.ArrayList;


public class Funcionario {
    private int codigo;
    private String nome;
    private int codigoCargo;
    private Cargo cargo;

    public Funcionario(int codigo, String nome, int codigoCargo) {
        this.codigo = codigo;
        this.nome = nome;
        this.codigoCargo = codigoCargo;
    }
    public void relatorio(ArrayList<Cargo> a) {
        System.out.println("-----------------");
        System.out.println("Codigo: " + this.codigo);
        System.out.println("Nome: " + this.nome);
        System.out.println("Salario: " + a.get(this.codigoCargo-1).getValor());
        System.out.println("-----------------");
    }
}
