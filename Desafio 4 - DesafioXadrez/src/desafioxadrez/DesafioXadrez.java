/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desafioxadrez;

import java.util.Arrays;

/**
 *
 * @author Davi Sisnando
 */
public class DesafioXadrez {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String[] names = {"Nulo", "Peão", "Bispo", "Torre", "Cavalo", "Rainha", "Rei"};
        int arrayFreq [] = new int[7];
        int[][] matriz = {
              {4, 3, 2, 5, 6, 2, 3, 4},
              {1, 1, 1, 1, 1, 1, 1, 1},
              {0, 0, 0, 0, 0, 0, 0, 0},
              {0, 0, 0, 0, 0, 0, 0, 0},
              {0, 0, 0, 0, 0, 0, 0, 0},
              {0, 0, 0, 0, 0, 0, 0, 0},
              {1, 1, 1, 1, 1, 1, 1, 1},
              {4, 3, 2, 5, 6, 2, 3, 4}
        };
        	
	
	for(int j = 0; j < matriz.length; j++){
            for(int k = 0; k < matriz.length; k++){
		++arrayFreq[matriz[j][k]]; //vai incrementar em 1 o valor da posição, por exemplo, primeiro valor seria: [0, 0, 0, 0, 1, 0, 0], ou seja 1 cavalo, proxima linha 1 torre (pos 3)
            }
	}
	
	// exibe resultado:
	/*for (int i=0; i<arrayFreq.length;i++){
            System.out.printf("%s: %d\n", names[i],arrayFreq[i]);  não consegui imprimir sem o nulo fazendo deste jeito :(
	}*/
        
        System.out.println("Peao: " + arrayFreq[1] + " peça(s)");
        System.out.println("Bispo: " + arrayFreq[2] + " peça(s)");
        System.out.println("Torre: " + arrayFreq[3] + " peça(s)");
        System.out.println("Cavalo: " + arrayFreq[4] + " peça(s)");
        System.out.println("Rainha: " + arrayFreq[5] + " peça(s)");
        System.out.println("Rei: " + arrayFreq[6] + " peça(s)");
    }
    
}
    
