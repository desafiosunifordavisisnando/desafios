/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indicede0;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 *
 * @author Davi Sisnando
 */
public class IndiceDe0 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
            ArrayList<Integer> vetor = new ArrayList<Integer>(
      Arrays.asList(0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1)
    );
            
    Collections.reverse(vetor);
    int index = -1;
    int maxCountOnes = -1;
    int countOnes = 0;

    for (int i = 0; i < vetor.size(); i++) {

      if (vetor.get(i) == 0) {
        for (int j = i + 1; j < vetor.size(); j++) {
          if (vetor.get(j) == 0) break;
          countOnes++;
        }
        
        if (maxCountOnes < countOnes) {
          maxCountOnes = countOnes;
          index = i;
        }

        countOnes = 0;
      } else {
        countOnes++;
      }
    }

    System.out.println("Posição para trocar o 0 por 1 é: " + index);

  }
}