/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maiornumero;

import java.util.Scanner;

/**
 *
 * @author Davi Sisnando
 */
public class MaiorNumero {
    
    /**
     * @param args the command line arguments
     */    
    public static void main(String[] args) {
        
        //método realizado segundo: https://www.maa.org/programs/faculty-and-departments/classroom-capsules-and-notes/the-maximum-and-minimum-of-two-numbers-using-the-quadratic-formula
        
        float firstNumber, secondNumber;
        Scanner scanner = new Scanner(System.in);
        System.out.println("digite o primeiro número: ");
        firstNumber = scanner.nextInt();
        System.out.println("digite o segundo número: ");
        secondNumber = scanner.nextInt();
        double resultado = ((firstNumber + secondNumber) / 2) + (Math.abs(firstNumber - secondNumber)) / 2;
        System.out.println("Maior número é: " + resultado);
        
        
        //outra maneira de fazer
        
        System.out.println("Maior número é: " + Math.max(firstNumber, secondNumber)); 
    }
}
