var app = new function(){
    this.el = document.getElementById('titulos') //Pegando o tbody e passando pro elemento, pra quando pegar novas tasks poder colocar na table
    this.titulos=[]

    this.Pegar = function(){
        var data=''
        console.log(titulos)
        if(this.titulos.length > 0){  //Se tiver itens na array, entao display data
            for(let i=0; i<this.titulos.length; i++){
                data+='<tr>';  //nova table row
                data+='<td class="linha-tabela">' + (i+1)+'. ' + this.titulos[i] + '</td>';  //nova celula, i+1 pra não mostrar linha 0
                data+='<td class="linha-tabela"><button onclick="app.Edit('+i+')" class ="btn-edit">Edit</button></td> ';
                data+='<td class="linha-tabela"><button onclick="app.Delete('+i+')" class ="btn-delete">Delete</button></td> '
                data+='</tr>'
            }
        }
        this.Count(this.titulos.length);
        return this.el.innerHTML = data
    }

    this.Add = function(){
        el = document.getElementById('nome')
        var nome = el.value
        if(nome){
            this.titulos.push(nome.trim());
            el.value='';
            this.Pegar();
        }
    }

    this.Edit = function(titulo){
        var el = document.getElementById('edit-nome')
        el.value = this.titulos[titulo]
        document.getElementById('edit-box').style.display = 'block';
        self = this

        document.getElementById('save').onsubmit = function(){
            var nome = el.value;
            if(nome) {
                self.titulos.splice(titulo, 1, nome.trim())
                self.Pegar()
                CloseInput()
            }
        }
    }

    this.Delete = function(titulo){
        this.titulos.splice(titulo, 1)
        this.Pegar()
    }

    this.Count = function(data){
        var el = document.getElementById('counter')
        var nome = 'Títulos'
        if(data){
            if(data == 1){
                nome = 'Título'
            }
            el.innerHTML = data+' ' + nome;
        } else {
            el.innerHTML = "Não há " + nome;
        }
    }
}

app.Pegar();

function CloseInput(){
    document.getElementById('edit-box').style.display = 'none';

}