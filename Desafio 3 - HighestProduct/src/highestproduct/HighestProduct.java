/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package highestproduct;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Davi Sisnando
 */
public class HighestProduct {
//Segundo max subarray product problem, https://en.wikipedia.org/wiki/Maximum_subarray_problem
//Esse programa dá exatamente a saída como mostrado no desafio

    /*public static int maiorProduto(int[] num) {

        int max = num[0];
        int start = 0;
        int end = 0;
        for(int i = 0; i < num.length; i++) {   //percorrer vetor
            for(int j = i; j < num.length; j++) { //percorrer do final
                int multiplicar = 1;
                for(int k = i; k <= j; k++) {   //multiplicar do começo ao final
                    multiplicar *= num[k];
                }
                if(max < multiplicar) {    //se existir uma multiplicação maior que essa, max vai ser o novo multiplicar, começo recebe posição do i(frente pra tras), end recebe posição de j (tras pra frente)
                    max = multiplicar;
                    start = i;
                    end = j;
                }
            }
        }

        System.out.print("O subarray é {");
        for(int i = start; i <= end; i++) {
            System.out.print(num[i]);
            if(i < end) {
                System.out.print(", ");
            }
        }
        System.out.println("}");

        return max;
    }

    public static void main(String[] args) {
        int[] array = { -6, 4, -5, 8, -10, 0, 8} ;
        int max = maiorProduto(array);
        System.out.println("Valor do produto = " + max);
        System.out.println("Código abaixo está do jeito que eu entendi que era pra ser feito, depois de tirar dúvidas pelo discord. \n Favor comentar a parte de cima e descomentar a de baixo para testar :)");
    }   */
    
    
    
    
    
    
    public static void main(String[] args) {
    int resultado = 1;
    int[] array = { -6, 4, -5, 8, -10, 0, 8};
    int[] negativeArray = new int[array.length];
    Arrays.sort(array);
    int[] filteredArray = Arrays.stream(array).filter(num -> num != 0).toArray();  //remover 0 da array
    for(int i = 0; i < filteredArray.length; i++) {
        if (filteredArray[i] < 0){
            negativeArray[i] = filteredArray[i];
        }
    }
    negativeArray = Arrays.stream(negativeArray).filter(num -> num != 0).toArray(); // remover 0 array negativa
    int[] arrayPositiva = Arrays.stream(array).filter(num -> num > 0).toArray(); // pegar todos os números da array positivos (>0)
    List<Integer> list = Arrays.stream(negativeArray).boxed().collect(Collectors.toList());
    if(list.size() %2 != 0){
        list.remove(list.size()-1);
    }
    
    for(int i = 0; i < arrayPositiva.length; i++){
        resultado *= arrayPositiva[i];
    }
    
    for(int i = 0; i < list.size(); i++){
        resultado *= list.get(i);
    }
    System.out.println("Output números positivos: " + (Arrays.toString(arrayPositiva)) + " Output números negativos: " + ((Arrays.toString(list.toArray()))));
    System.out.println("Resultado maior produto possível dentre os números informados: " + resultado);
    System.out.println("\n" + "Se quiser testar com outra array, mudar diretamente no código fonte, na segunda linha. Obrigado!");
    
    }
}